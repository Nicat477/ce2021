using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BallPhysics : MonoBehaviour
{
    Rigidbody r;
    AudioSource a;

    [SerializeField]
    TextMeshProUGUI health;
    [SerializeField]
    TextMeshProUGUI armour;

    int kickCount;

    float speed = 0f;
    void Start()
    {
        r = GetComponent<Rigidbody>();
        a = GetComponent<AudioSource>();
    }

    void Update()
    {
        speed = 15;
    }

    private void FixedUpdate()
    {
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name=="Plane")
        {
            kickCount++;
            health.text = kickCount.ToString();
            //a.Play();
        }
    }
}
