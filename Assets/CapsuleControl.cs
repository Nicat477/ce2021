using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleControl : MonoBehaviour
{
    CharacterController c;
    float dx, dz;
    [SerializeField]
    float speed;

    [SerializeField]
    float gravity;

    [SerializeField]
    float jumpHeigt;

    bool jump;

    Vector3 movement = Vector3.zero;

    void Start()
    {
        c = GetComponent<CharacterController>();
    }

    void Update()
    {
        //input
        dx = Input.GetAxis("Horizontal");
        dz = Input.GetAxis("Vertical");
        if(!jump && Input.GetKeyDown(KeyCode.Space))
            jump = true;
    }

    private void FixedUpdate()
    {
        //movement logic
        movement = new Vector3(dx, 0, dz) * speed * Time.fixedDeltaTime;

        //jumping logic
        if (jump)
        {
            movement.y = jumpHeigt;
            jump = false;
        }

        //gravity logic
        if (jump && c.isGrounded)
            movement.y = 0;
        else
            movement.y -= gravity;


        c.Move(movement);

        var mousepos = Input.mousePosition;
        var capsulePos = Camera.main.WorldToScreenPoint(transform.position);
        var dir = capsulePos - mousepos;

        var angle = Mathf.Atan2(dir.y,dir.x) *Mathf.Rad2Deg;
        
        transform.rotation = Quaternion.AngleAxis(-angle -90, Vector3.up);
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    Debug.Log(other.gameObject.name + "Entered");
    //}

    //private void OnTriggerExit(Collider other)
    //{
    //    Debug.Log(other.gameObject.name + "Exit");
    //}


    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.name == "football")
        {
            var a = (hit.point - transform.position).normalized;
            a.y = 0;
            hit.gameObject.GetComponent<Rigidbody>().AddForce(a.normalized * 10, ForceMode.Impulse);
        }
    }
}
